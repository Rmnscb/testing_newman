FROM postman/newman:alpine

RUN npm i -g newman-reporter-confluence && \
    npm i -g newman-reporter-junitxray && \
    npm i -g newman-reporter-htmlextra && \
	npm i -g newman-reporter-junitfull	


